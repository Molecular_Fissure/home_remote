package processing.test.room_android;

import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Room_android extends PApplet {

int padH = height/8;
int padD = height/8;
int padL = width/4;
int padR = width/4;
button fan,table,bulb,bed;

public void setup(){
   
  padH = height/8;
  padD = height/8;
  padL = width/4;
  padR = width/4;
  fan = new button(width/2,height/2,200,200);
  bulb = new button(0,padH,padL,height-padD-padD);
  table = new button(0,0,width,padH);
  bed = new button(0,height-padD,width,padD);
  mouseX=-1;mouseY=-1;
}

public void draw(){
  background(0,35,132);  
  fan.display();  
  bulb.display();
  table.display();
  bed.display();  
  fan.mousePressed();
  fan.mouseReleased();
  text(fan.c,width/2, height/2+300);
  text(mouseX,width/2, height/2+350);
  text(mouseY,width/2, height/2+400);
  text(PApplet.parseInt(fan.t),width/2, height/2+450);
  text(PApplet.parseInt(fan.mHover),width/2, height/2+500);
}

/*void mousePressed(){
    if(fan.mHover==true){
      fan.t=true;table.t=false;bulb.t=false;bed.t=false;
      table.mHover=false;bulb.mHover=false;bed.mHover=false;
    }
    if(table.mHover==true){
      fan.t=false;table.t=true;bulb.t=false;bed.t=false;
      fan.mHover=false;bulb.mHover=false;bed.mHover=false;
    }
    if(bulb.mHover==true){
      fan.t=false;table.t=false;bulb.t=true;bed.t=false;
      table.mHover=false;fan.mHover=false;bed.mHover=false;
    }
    if(bed.mHover==true){
      fan.t=false;table.t=false;bulb.t=false;bed.t=true;
      table.mHover=false;bulb.mHover=false;fan.mHover=false;
    }
}

void mouseReleased(){
  fan.t=false;
  table.t=false;
  bulb.t=false;
  bed.t=false;
  mouseX=-1;mouseY=-1;
}
*/

  
class button{
  boolean s =false, t=false, mHover=false;
  float c;
  int x,y,w,h,time=millis();
  int color1,color2,col;
  int on1=color(0,150,0);
  int on2=color(0,255,0);
  int off1=color(150,0,0);
  int off2=color(255,0,0);
  
button(int xx, int yy, int ww, int hh){
  x=xx;
  y=yy;
  w=ww;
  h=hh;
  col=0;
  color1=off1;
  color2=off2;
  c=0;
}

public void mousePressed(){mHover=true;
  if(mouseX>=x && mouseX<=x+w
    && mouseY>=y && mouseY<=y+h){t=true;}}
  public void mouseReleased(){mHover=false;t=false;}
  
public void display(){
  c=constrain(c,0,0.9f);
  col = lerpColor(color1,color2,c);
  
  fill(col);
  noStroke();
  rect(x,y,w,h);
  textSize(32);
  
  if(t){c+=0.1f;
    if(c>0.9f && s==false){s=true;}
          else if(c>0.9f && s==true){s=false;}
   }
          
    if(!t){c-=0.1f;}
  
  if(mouseX>=x && mouseX<=x+w
    && mouseY>=y && mouseY<=y+h){
      mHover = true;
      //c+=0.1; 
               
    if(s){color1=on1;color2=on2;}
    if(!s){color1=off1;color2=off2;}    
    }
    
    else{mHover=false;//c-=0.1;
  }
} 
  
  
  public boolean Switch(){return s;}
}
  public void settings() {  fullScreen(); }
}
